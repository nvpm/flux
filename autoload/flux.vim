" auto/flux.vim
" once                                                                         {

if exists('__FLUXAUTO__')|finish|endif
let __FLUXAUTO__=1|

" end-once }
" func                                                                         {

fu! flux#flux(...) " the main flux function                                    {

  if empty(a:000)||type(a:1)!=type({})|return|else|let conf=a:1|endif

  call flux#read(conf)
  call flux#endl(conf)
  call flux#trim(conf)
  call flux#list(conf)
  call flux#cuts(conf)
  call flux#loop(conf)
  call flux#home(conf)
  retu flux#data(conf)

endfu "}
fu! flux#data(...) " returns flux-tree data structure                          {

  if empty(a:000)||type(a:1)!=type({})|return {}|else|let conf=a:1|endif

  let list = get(conf,'list',[])
  let home = get(conf,'HOME','')
  let leng = get(conf,'leng',len(list))

  " leave conf the way it was
  if has_key(conf,'leng')|unlet conf.leng|endif
  if has_key(conf,'list')|unlet conf.list|endif
  if has_key(conf,'body')|unlet conf.body|endif
  if has_key(conf,'HOME')|unlet conf.HOME|endif

  "call flux#show(tree)

  return flux#tree(list,home,leng)

endfu "}
fu! flux#tree(...) " builds the tree out of the conf.list of nodes             {

  let list = get(a:000,0,[])
  let home = get(a:000,1,'')
  let leng = get(a:000,2,len(list))
  let indx = 0

  let tree = #{list:[],leng:0,indx:0}

  while indx<leng

    " catches node from given nodelist
    let node = list[indx]|let indx+=1

    " recursively handles sub-tree nodes
    if 1+node.tree
      let init = indx
      while indx<leng
        let item = list[indx]
        " breaks at next same (or higher) depth node
        if 1+item.tree && item.tree <= node.tree
          break
        endif
        let indx+=1
      endwhile

      " handles home functionality
      let home = empty(home)?'.':home
      if has_key(node,'home')
        if !has_key(node,'@')
          let node.home = home.'/'.node.home
        else
          let node.home = node.home
        endif
      endif

      " beautify path
      let node.home = substitute(node.home,'//','/','g')
      let node.home = substitute(node.home,'/$','','')

      " extend node fields with list, indx and leng, recursively
      call extend(node,flux#tree(list[init:indx-1],node.home,indx-init))

    endif

    " skips non-matching nodes
    if node.deep<0|continue|endif

    " handles cut-tree functionality
    if node.cuts==1|continue|endif
    if node.cuts==2| break  |endif

    " a leafless sub-tree perishes
    if empty(get(node,'list','leafless'))|continue|endif

    " handles home functionality for leaf nodes
    if has_key(node,'home')&&-1==node.tree&&!has_key(node,'@')
      let node.home = home
    endif

    " removes obsolete fields
    if has_key(node,'@')   |unlet node['@']|endif
    if has_key(node,'cuts')|unlet node.cuts|endif
    if has_key(node,'tree')|unlet node.tree|endif

    " finally adds node to tree & increment tree length
    call add(tree.list,node)|let tree.leng+=1

  endwhile

  return tree

endfu "}
fu! flux#node(...) " transforms a line into a node                             {

  let line = get(a:000,0,'')
  let tree = get(a:000,1,'')
  let leaf = get(a:000,2,'')

  let line = ['',line][type(line)==type('')]

  let rgex = '^\v *(-*) *(\w*) *(.*)$'
  let info = matchlist(line,rgex)

  let cuts = info[1]
  let keyw = info[2]
  let info = info[3]

  let node = {}
  let rgex = '\v *[=:@] *'

  " stores absolute key for home absolute path functionality
  let absl = trim(matchstr(info,rgex))
  if absl=='@'|let node['@']=1|endif

  " split info into name and info again
  let info = split(info,rgex,1)
  if len(info)==1
    let name = trim(info[0])
    let info = ''
  elseif len(info)>=2
    let name = trim(info[0])
    let info = trim(info[1])
  endif

  let node.cuts = len(cuts) " numberfy cuts
  let node.keyw = keyw
  let node.name = name
  let node.info = info
  let node.tree = flux#find(tree,node.keyw)
  let node.deep = flux#find(tree..'|'..leaf,node.keyw)

  return node

endfu "}
fu! flux#find(...) " finds a keyword's index given a wordlist                  {

  if !exists('a:1')||type(a:1)!=type('')|return -1|endif
  if !exists('a:2')||type(a:2)!=type('')|return -1|endif

  let strg = a:1
  let word = a:2
  let keyw = ''
  let indx = 0

  for char in strg
    if char =~ '\w'|let keyw.=char|continue|endif
    if char =~ '\s'
      if keyw==?word|return indx|endif
      let keyw=''
      continue
    endif
    if char == '|'
      if keyw==?word|return indx|endif
      if !empty(keyw)|let indx+=1|let keyw=''|else|continue|endif
    endif
  endfor

  return [-1,indx][keyw==?word]

endfu "}
fu! flux#show(...) " shows flux-tree structure                                 {

  let tree = get(a:000,0,{})
  let step = get(a:000,1, 0)
  let tabs = repeat(' ',step)
  let leng = get(tree,'leng',0)
  while leng
    let node = tree.list[-leng]|let leng-=1
    let info = has_key(node,'info')?node.info:node.home
    let char = [' : ',''][empty(info)]
    ec tabs node.deep node.keyw node.name..char..info
    if has_key(node,'list')
      call flux#show(node,step+2)
    endif
  endwhile

endfu "}

fu! flux#read(...) " reads conf.file if it is present                          {

  if empty(a:000)||type(a:1)!=type({})|return|else|let conf=a:1|endif

  if has_key(conf,'file')
    if type(conf.file)==type('')
      if filereadable(conf.file)
        let conf.body = readfile(conf.file)
      endif
    endif
  endif
  " makes sure it's a body of string-lines
  if !has_key(conf,'body')|let conf.body=[]|else
    call filter(conf.body,'type(v:val)==type("")')
  endif

endfu "}
fu! flux#endl(...) " splits lines by endl char                                 {

  if empty(a:000)||type(a:1)!=type({})|return|else|let conf=a:1|endif
  if has_key(conf,'body')
    let endl = get(conf,'endl')
    let endl = [',',endl][type(endl)==type('')]
    let endl = '\m\s*'..endl..'\s*'
    let leng = get(conf,'leng',len(conf.body))|let conf.leng=0
    let indx = 0
    let body = []
    while indx<leng
      let line = conf.body[indx]|let indx+=1
      let line = split(line,endl)
      call extend(body,line)
      let conf.leng+=len(line)
    endwhile
    let conf.body = body
  endif

endfu "}
fu! flux#trim(...) " trims-out comments and empty lines                        {

  if empty(a:000)||type(a:1)!=type({})|return|else|let conf=a:1|endif
  if has_key(conf,'body')
    let comm = get(conf,'comm')
    let comm = ['#',comm][type(comm)==type('')]
    let comm = '\m\s*'..comm..'.*'
    let leng = get(conf,'leng',len(conf.body))|let conf.leng=0
    let indx = 0
    let body = []
    while indx<leng
      let line = conf.body[indx]
      let line = trim(substitute(line,comm,'',''))
      if !empty(line)|call add(body,line)|let conf.leng+=1|endif
      let indx+=1
    endwhile
    let conf.body = body
  endif

endfu "}
fu! flux#list(...) " transforms conf.body into a list of nodes                 {

  if empty(a:000)||type(a:1)!=type({})|return|else|let conf=a:1|endif

  let conf.list = []
  if has_key(conf,'body')
    let tree = get(conf,'tree','')
    let leaf = get(conf,'leaf','')
    let leng = get(conf,'leng',len(conf.body))
    let indx = 0
    while indx<leng
      let node = flux#node(conf.body[indx],tree,leaf)
      call add(conf.list,node)
      let indx+= 1
    endwhile
    unlet conf.body
    let conf.leng = indx
  endif

endfu "}
fu! flux#cuts(...) " handles standalone cuts & cut3+                           {

  if empty(a:000)||type(a:1)!=type({})|return|else|let conf=a:1|endif

  if has_key(conf,'list')
    let leng = get(conf,'leng',len(conf.list))
    let indx = 0
    let cuts = 0
    let list = []
    let conf.leng = 0
    while indx<leng
      let node = conf.list[indx]|let indx+=1
      " breaks at cut3+
      if node.cuts>=3|break|endif
      " pushes forward current cut info if stand-alone
      if node.cuts&&empty(node.keyw..node.name..node.info)
        let cuts      = node.cuts
        let node      = conf.list[indx]|let indx+= 1
        let node.cuts = cuts
      endif
      call add(list,node)|let conf.leng+=1
    endwhile
    let conf.list = list
  endif

endfu "}
fu! flux#loop(...) " handles loop functionality                                {

  if empty(a:000)||type(a:1)!=type({})|return|else|let conf=a:1|endif

  if has_key(conf,'list')
    let indx = 0
    let list = []
    let leng = get(conf,'leng',len(conf.list))
    let conf.leng = 0
    while indx<leng
      let node = conf.list[indx]|let indx+=1
      if node.keyw !=? 'loop'|call add(list,node)|let conf.leng+=1|else
        let loop = []
        while indx<leng
          let item = conf.list[indx]|let indx+=1
          if item.keyw==?'endloop'|break|endif
          call add(loop,item)
        endwhile
        let name = node.name
        let vars = split(node.info,' ')
        for var in vars
          " cut-tree for loop-vars
          let cuts = flux#node(var).cuts
          if cuts==1|continue|endif
          if cuts==2|  break |endif
          if empty(var)|continue|endif
          for item in loop
            let info      = copy(item)
            let info.name = substitute(info.name,'$'.name,var,'g')
            let info.info = substitute(info.info,'$'.name,var,'g')
            let info.cuts = node.cuts
            call add(list,info)|let conf.leng+=1
          endfor
        endfor
      endif
    endwhile
    let conf.list = list
  endif

endfu "}
fu! flux#home(...) " handles home functionality                                {

  if empty(a:000)||type(a:1)!=type({})|return|else|let conf=a:1|endif

  if !has_key(conf,'home')|return|endif

  if has_key(conf,'list')
    let indx = 0
    let leng = get(conf,'leng',len(conf.list))
    let list = []
    let conf.leng = 0
    while indx<leng
      let node = conf.list[indx]|let indx+=1
      if node.keyw==?'home'
        if node.cuts==1|continue|endif
        if node.cuts==2|  break |endif
        let conf.HOME = [node.info,node.name][empty(node.info)]
        continue
      endif
      let node.home = 1+node.tree?remove(node,'info'):''
      call add(list,node)|let conf.leng+=1
    endwhile
    let conf.list = list
  endif

endfu "}

" end-func }
